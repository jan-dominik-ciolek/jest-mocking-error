import * as FakeModule from '../FakeModule'

// @ts-ignore
@FakeModule.Decorator()
class Test {
    method = () => {

    }
}

export { 
    Test
}