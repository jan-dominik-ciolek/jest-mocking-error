import { Test } from './Service'

jest.mock('./FakeModule', () => {

    return {
        ...jest.requireActual('./FakeModule'),
        someRandomMockedProperty:"mockedValue",
        __esModule:true
    }
})



describe("Fake test", () => {

    it('fake test', () => {
        const test = new Test()
        test.method()
        expect(true).toBe(true)
    })

})